from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoe


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name",
        "picture_url",
    ]


class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "model_name",
        "color",
        "manufacturer",
        "picture_url",
        "bin",
    ]


@require_http_methods(["GET", "POST"])
def api_shoes_list(request):
    if request.method == "GET":
        try:
            shoes = Shoe.objects.all()
            shoe_list = [ShoeListEncoder().default(shoe) for shoe in shoes]
            return JsonResponse({"shoes": shoe_list}, safe=False)
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            return response
    elif request.method == "POST":
        content = json.loads(request.body)
        shoe = Shoe.objects.create(**content)
        return JsonResponse(ShoeEncoder().default(shoe))


@require_http_methods(["GET", "DELETE", "PUT"])
def api_ShoeDetails(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not Exist"})
            response.status_code = 404
            return response
    elif request.method == "PUT":
        try:
            shoe = Shoe.objects.get(id=pk)
            content = json.loads(request.body)
            for key, value in content.items():
                setattr(shoe, key, value)
            shoe.save()
            return JsonResponse(
                shoe,
                encoder=ShoeEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(shoe, encoder=ShoeEncoder, safe=False)
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
