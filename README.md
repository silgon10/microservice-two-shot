# Wardrobify

Team:

* An Tran - Which microservice? HATS
* Silvano Gonzalez - Which microservice? SHOES
- An - Hats
- Silvano - Shoes

## Design

## Shoes microservice

Make shoe model. Make restful views for shoes that tie them to shoes closet and assign shoes to a certain shoe bin. Establish the shoes urls.

use react to make a page for shoes list and all details. \*\* make a delete button to delete shoes.

use react to make a form to create a new shoe entry

## Hats microservice

Make hat model that tracks fabric, style name, color, url and location in the wardrobe API endpoint.

Make React component that renders hat page. Include a table that shows the details of each hat and a form to submit a new hat.
