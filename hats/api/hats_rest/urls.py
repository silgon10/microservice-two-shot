from django.urls import path

from .views import hats_view


urlpatterns = [
    path("hats/", hats_view, name="hats_view"),
    path ("hats/<int:hat_id>/", hats_view, name="hats_view_with_id")
]
