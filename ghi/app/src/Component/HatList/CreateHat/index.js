import './index.css'
import { useState, useEffect } from 'react';

function CreateHat({refreshData, setRefreshData}) {


    const [newHatData, setNewHatData] = useState({
        fabric: '',
        style_name: '',
        color: '',
        url: '',
        location: ''
    });


    const handleSubmit = (event) => {
    event.preventDefault();

    const apiUrl = 'http://localhost:8090/api/hats/';
    fetch(apiUrl, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        },
        body: JSON.stringify(newHatData),
    })
    .then(() => {
    setNewHatData({
        fabric: '',
        style_name: '',
        color: '',
        url: '',
        location: ''
    });
    setRefreshData(!refreshData)
    })
    .catch((error) => console.error('Error creating hat:', error));
    };

    return (
        <>
            <form onSubmit={handleSubmit}>
                <label>
                    Fabric:
                    <input
                    type="text"
                    value={newHatData.fabric}
                    onChange={(e) => setNewHatData({ ...newHatData, fabric: e.target.value })} />
                </label>
                <label>
                    Style Name:
                    <input
                    type="text"
                    value={newHatData.style_name}
                    onChange={(e) => setNewHatData({ ...newHatData, style_name: e.target.value })} />
                </label>
                <label>
                    Color:
                    <input
                    type="text"
                    value={newHatData.color}
                    onChange={(e) => setNewHatData({ ...newHatData, color: e.target.value })} />
                </label>
                <label>
                    URL:
                    <input
                    type="text"
                    value={newHatData.url}
                    onChange={(e) => setNewHatData({ ...newHatData, url: e.target.value })} />
                </label>
                <label>
                    location:
                    <input
                    type="text"
                    value={newHatData.location}
                    onChange={(e) => setNewHatData({ ...newHatData, location: e.target.value })} />
                </label>
                {/* <label>
                    Closet Name:
                    <input type="text" value={newHatData.location.closet_name} onChange={(e) => setNewHatData({ ...newHatData, location: { ...newHatData.location, closet_name: e.target.value } })} />
                </label>
                <label>
                    Section Number:
                    <input type="text" value={newHatData.location.section_number} onChange={(e) => setNewHatData({ ...newHatData, location: { ...newHatData.location, section_number: e.target.value } })} />
                </label>
                <label>
                    Shelf Number:
                    <input type="text" value={newHatData.location.shelf_number} onChange={(e) => setNewHatData({ ...newHatData, location: { ...newHatData.location, shelf_number: e.target.value } })} />
                </label> */}
                <button type="submit">Add Hat</button>
            </form>
        </>
    )
}


export default CreateHat
