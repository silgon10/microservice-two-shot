import './index.css';
import { useState, useEffect } from 'react';
import CreateHat from './CreateHat';

function Hats() {
    const [data, setData] = useState([]);
    const [refreshData, setRefreshData] = useState(true)

    useEffect(() => {
        const apiUrl = 'http://localhost:8090/api/hats/';
        console.log("useEffect")
        fetch(apiUrl)
            .then(response => response.json())
            .then(data => setData(data["hatList"]))
            .catch(error => console.error('Error fetching data:', error ))
    }, [refreshData])

    const TableRow = ({eachHat}) => {
        return (
            <>
                <tr>
                    <td>{eachHat.fabric}</td>
                    <td>{eachHat.style_name}</td>
                    <td>{eachHat.color}</td>
                    <td>{eachHat.url}</td>
                    <td>{eachHat.location.closet_name}</td>
                    <td>{eachHat.location.section_number}</td>
                    <td>{eachHat.location.shelf_number}</td>
                    <td className='delete-btn'><button onClick={() => handleDelete(eachHat.id)}>x</button></td>
                </tr>
            </>

        );
    };

    const handleDelete = (id) => {
        const apiUrl = `http://localhost:8090/api/hats/${id}/`;

        fetch(apiUrl, {
        method: 'DELETE',
        })
        .then((response) => {
            if (!response.ok) {
            throw new Error('Network response was not ok');
            }
            setData((prevData) => prevData.filter((hat) => hat.id !== id));
        })
        .catch((error) => console.error('Error deleting data:', error));
    };

    return (
        <div>
            <table>
                    <tr>
                        <th>Fabric</th>
                        <th>Style Name</th>
                        <th>Color</th>
                        <th>URL</th>
                        <th>Closet Name</th>
                        <th>Section Number</th>
                        <th>Shelf Number</th>
                    </tr>

                    {
                        data.map(eachHat => {
                            return (
                                <TableRow key={eachHat.id} eachHat={eachHat}/>
                            )
                        })
                    }
            </table>

            <CreateHat refreshData={refreshData} setRefreshData={setRefreshData}/>

        </div>
    )
}


export default Hats
