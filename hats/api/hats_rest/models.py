from django.db import models


class HatLocation(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField(blank=True, null=True)
    shelf_number = models.PositiveSmallIntegerField(blank=True, null=True)
    import_href = models.CharField(max_length=300, null=True, blank=True, unique=True)

# Create your models here.
class HatList(models.Model):
    fabric = models.CharField(max_length=200, blank=True, null=True)
    style_name = models.CharField(max_length=200, blank=True, null=True)
    color = models.CharField(max_length=200, blank=True, null=True)
    url = models.URLField(max_length=200, blank=True, null=True)
    location = models.ForeignKey(
        HatLocation,
        related_name='HatList',
        on_delete=models.PROTECT,
    )
