from django.urls import path
from .views import api_shoes_list, api_ShoeDetails

urlpatterns = [
    path("shoes/", api_shoes_list, name="shoes_list"),
    path("shoes/<int:pk>/", api_ShoeDetails, name="shoe_details"),
]
